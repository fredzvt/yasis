﻿using System;
using UnityEngine;

public class CameraCraneScript : MonoBehaviour
{
    public Transform playerShip;
    public float rotationVelocity = 1f;

    private bool paused = false;

    public void OnGUI()
    {
    }

    public void Start()
    {
    }

    public void Pause()
    {
        paused = true;
    }

    public void Unpause()
    {
        paused = false;
    }

    public void Update()
    {
        if (!paused)
        {
            var currPlayerPositionPercentual = (playerShip.transform.position.x * 100) / GameSettings.PlayerHorizontalRange;
            var newAngle = (GameSettings.CameraCraneRotationRange * currPlayerPositionPercentual) / 100f;
            var currRotation = transform.rotation.eulerAngles;
            var newRotation = Quaternion.Euler(currRotation.x, newAngle, currRotation.z);
            var lerpedRotation = Quaternion.Lerp(transform.localRotation, newRotation, Time.deltaTime * rotationVelocity);
            transform.localRotation = lerpedRotation;
        }
    }
}
