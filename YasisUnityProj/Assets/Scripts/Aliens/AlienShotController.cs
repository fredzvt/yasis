﻿using System;
using UnityEngine;

public delegate void AlienShotDestroyedHandler(AlienShotController shot);
public delegate void AlienShotHitPlayerShipHandler(AlienShotController shot);

public class AlienShotController : MonoBehaviour
{
    public float Velocity = 1f;

    public event AlienShotDestroyedHandler OnDestroyed;
    public event AlienShotHitPlayerShipHandler OnHitPlayerShip;

    private bool paused = false;

    public void Update()
    {
        if (!paused)
        {
            var newPos = transform.position.Copy();
            newPos.y -= Velocity;
            transform.position = newPos;

            if (transform.position.y < GameSettings.ScreenBottom)
            {
                DestroyMe();
            }
        }
    }

    public void Pause()
    {
        paused = true;
    }

    public void Unpause()
    {
        paused = false;
    }

    public void DestroyMe()
    {
        if (OnDestroyed != null)
            OnDestroyed(this);

        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerShip")
        {
            if (OnHitPlayerShip != null)
                OnHitPlayerShip(this);

            DestroyMe();
        }
    }
}