﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public delegate void AlienShipDestroyedHandler(BaseAlienShipController alienShip);
public delegate void AlienShipShootDelegate(AlienShotController shot);

public abstract class BaseAlienShipController : MonoBehaviour
{
    public event AlienShipShootDelegate OnShipShoot;
    public event AlienShipDestroyedHandler OnDestroyed;

    public float MovementDamping = 10f;
    public bool debug = false;
    public GameObject explosion;
    public Light explosionLight;
    public GameObject[] disableOnDestroy;
    public int level = 1;
    public GameObject shotPrefab;
    public Transform shotSpawnPoint;

    protected Vector3 targetPosition;
    private bool wrappingEnabled = false;
    private bool paused = false;
    private bool destroyed = false;
    private bool stopped = false;
    private Animator[] animators;
    private Collider[] colliders;
    private bool cannonLocked = false;
    private System.Random rnd;

    private static int rndSeed = (int)DateTime.Now.Ticks;

    public void Awake()
    {
        if (explosion == null) throw new Exception("Explosion is null.");
        if (explosionLight == null) throw new Exception("Explosion light is null.");
        if (shotPrefab == null) throw new Exception("shotPrefab is null.");
        if (shotSpawnPoint == null) throw new Exception("shotSpawnPoint is null.");

        rnd = new System.Random(++rndSeed);
        targetPosition = transform.position.Copy();
        animators = gameObject.GetComponentsInChildren<Animator>();
        colliders = gameObject.GetComponentsInChildren<Collider>();
    }

    public void DestroyMe()
    {
        StartCoroutine("ExplodeAndDestroy");
    }

    private IEnumerator ExplodeAndDestroy()
    {
        destroyed = true;

        if (OnDestroyed != null)
            OnDestroyed(this);

        foreach (var obj in disableOnDestroy)
            obj.SetActive(false);

        explosion.SetActive(true);
        explosionLight.gameObject.SetActive(true);

        explosionLight.intensity = 0;
        var seq = DOTween.Sequence();
        seq.Append(DOTween.To(() => explosionLight.intensity, (x) => explosionLight.intensity = x, 8f, .2f));
        seq.Append(DOTween.To(() => explosionLight.intensity, (x) => explosionLight.intensity = x, 0f, .2f));
        seq.Play();

        foreach (var collider in colliders)
            collider.enabled = false;

        yield return new WaitForSeconds(.3f);

        Destroy(gameObject);
    }

    public void Update()
    {
        if (!paused && !destroyed)
        {
            if (!stopped)
            {
                UpdateTargetPosition();
                UpdateCannon();
            }

            transform.position =
                Vector3.Lerp(
                    transform.position,
                    targetPosition,
                    MovementDamping * Time.deltaTime
                );
            
            if (!stopped)
            {
                WrapPosition();
            }

            if (debug)
            {
                DebugPosition();
            }
        }
    }

    public void Stop()
    {
        stopped = true;
    }

    public void Pause()
    {
        paused = true;

        if (animators != null && animators.Length > 0)
            foreach (var anim in animators)
                anim.speed = 0;
    }

    public void Unpause()
    {
        paused = false;

        if (animators != null && animators.Length > 0)
            foreach (var anim in animators)
                anim.speed = 1;
    }

    private void UpdateCannon()
    {
        if (!cannonLocked)
        {
            if (rnd.Next(1, 500) == 1)
            {
                var shot = (GameObject)Instantiate(shotPrefab, shotSpawnPoint.position, Quaternion.identity);

                var shotController = shot.GetComponent<AlienShotController>();
                if (shotController == null)
                    throw new Exception("ShotPrefab does not have a AlienShotController component.");

                if (OnShipShoot != null)
                    OnShipShoot(shotController);

                cannonLocked = true;
                StartCoroutine("CannonCooldown");
            }
        }
    }

    private IEnumerator CannonCooldown()
    {
        yield return new WaitForSeconds(1);
        cannonLocked = false;
    }

    private void DebugPosition()
    {
        Utils.DebugDrawCrossAtPosition(targetPosition, Color.green);
        Utils.DebugDrawCrossAtPosition(transform.position, Color.yellow);
    }

    private enum HorizontalWrap { LeftToRight, RightToLeft }
    private enum VerticalWrap { TopToBottom, BottomToTop }
    private void WrapPosition()
    {
        if (!wrappingEnabled)
        {
            // Alien ships just start to wrap screen once they were visible for the first time.

            wrappingEnabled =
                transform.position.x >= GameSettings.ScreenLeft &&
                transform.position.x <= GameSettings.ScreenRight &&
                transform.position.y >= GameSettings.ScreenBottom &&
                transform.position.y <= GameSettings.ScreenTop;
        }
        else
        {
            HorizontalWrap? horWrap = null;
            VerticalWrap? vertWrap = null;
            float bleedX = 0, bleedY = 0;

            if (transform.position.x > GameSettings.ScreenRight)
            {
                bleedX = transform.position.x - GameSettings.ScreenRight;
                horWrap = HorizontalWrap.RightToLeft;
            }
            else if (transform.position.x < GameSettings.ScreenLeft)
            {
                bleedX = GameSettings.ScreenLeft - transform.position.x;
                horWrap = HorizontalWrap.LeftToRight;
            }

            if (transform.position.y > GameSettings.ScreenTop)
            {
                bleedY = transform.position.y - GameSettings.ScreenTop;
                vertWrap = VerticalWrap.TopToBottom;
            }
            else if (transform.position.y < GameSettings.ScreenBottom)
            {
                bleedY = GameSettings.ScreenBottom - transform.position.y;
                vertWrap = VerticalWrap.BottomToTop;
            }

            if (horWrap.HasValue || vertWrap.HasValue)
            {
                float targetOffsetY = targetPosition.y - transform.position.y;
                float targetOffsetX = targetPosition.x - transform.position.x;

                float newYPos =
                    !vertWrap.HasValue
                    ? transform.position.y
                    : vertWrap == VerticalWrap.BottomToTop
                      ? GameSettings.ScreenTop - bleedY
                      : GameSettings.ScreenBottom + bleedY;

                float newXPos =
                    !horWrap.HasValue
                    ? transform.position.x
                    : horWrap == HorizontalWrap.LeftToRight
                      ? GameSettings.ScreenRight - bleedX
                      : GameSettings.ScreenLeft + bleedX;

                var newPos = new Vector3(newXPos, newYPos, transform.position.z);
                transform.position = newPos;

                targetPosition = newPos.Copy();
                targetPosition.y += targetOffsetY;
                targetPosition.x += targetOffsetX;
            }
        }
    }

    protected abstract void UpdateTargetPosition();
}