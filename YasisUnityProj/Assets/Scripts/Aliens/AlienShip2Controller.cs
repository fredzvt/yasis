﻿using System;
using UnityEngine;

public class AlienShip2Controller : BaseAlienShipController
{
    private enum VerticalState { FirstDescent, FirstStop, SecondDescent, SecondStop }
    private VerticalState vertState = VerticalState.FirstDescent;
    private float lastVertStateChangeTime = 0;
    private float lastHorChangeTime = 0;

    public float HorizontalVelocity = .5f;
    public float VerticalVelocity = -.5f;

    protected override void UpdateTargetPosition()
    {
        var verticalVelocityToApply = 0f;

        if (Time.time - lastHorChangeTime > 3)
        {
            lastHorChangeTime = Time.time;
            HorizontalVelocity *= -1;
        }

        switch (vertState)
        {
            case VerticalState.FirstDescent:
                verticalVelocityToApply = VerticalVelocity;
                if (Time.time - lastVertStateChangeTime > .1f)
                {
                    lastVertStateChangeTime = Time.time;
                    vertState = VerticalState.FirstStop;
                }
                break;

            case VerticalState.FirstStop:
                verticalVelocityToApply = 0;
                if (Time.time - lastVertStateChangeTime > .15f)
                {
                    lastVertStateChangeTime = Time.time;
                    vertState = VerticalState.SecondDescent;
                }
                break;

            case VerticalState.SecondDescent:
                verticalVelocityToApply = VerticalVelocity;
                if (Time.time - lastVertStateChangeTime > .1f)
                {
                    lastVertStateChangeTime = Time.time;
                    vertState = VerticalState.SecondStop;
                }
                break;

            case VerticalState.SecondStop:
                verticalVelocityToApply = 0;
                if (Time.time - lastVertStateChangeTime > 2)
                {
                    lastVertStateChangeTime = Time.time;
                    vertState = VerticalState.FirstDescent;
                }
                break;

            default:
                throw new Exception("Unexpected vertical state.");
        }

        targetPosition =
            new Vector3(
                targetPosition.x += HorizontalVelocity,
                targetPosition.y += verticalVelocityToApply,
                targetPosition.z
            );
    }
}