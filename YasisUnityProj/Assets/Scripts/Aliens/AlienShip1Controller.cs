﻿using System;
using UnityEngine;

public class AlienShip1Controller : BaseAlienShipController
{
    private float HorizontalVelocity = .3f;
    private float HorizontalVelocityToAddPerLevel = .1f;
    private float VerticalVelocity = -.005f;
    private float VerticalVelocityToAddPerLevel = -.001f;

    public void Start()
    {
        HorizontalVelocity += (HorizontalVelocityToAddPerLevel * (level - 1));
        VerticalVelocity += (VerticalVelocityToAddPerLevel * (level - 1));
    }

    protected override void UpdateTargetPosition()
    {
        targetPosition =
            new Vector3(
                targetPosition.x += HorizontalVelocity,
                targetPosition.y += VerticalVelocity,
                targetPosition.z
            );
    }
}