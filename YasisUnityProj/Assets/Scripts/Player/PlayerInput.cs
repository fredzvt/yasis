﻿using System;

public class PlayerInput
{
    public Direction? direction;
    public bool shoot;

    public override string ToString()
    {
        var dir = "None";

        if (direction.HasValue)
        {
            switch (direction.Value)
            {
                case Direction.Left:
                    dir = "Left";
                    break;

                case Direction.Right:
                    dir = "Right";
                    break;

                default:
                    throw new Exception("Unexpected direction.");
            }
        }

        return
            string.Format(
                "Direction: {0}, Shoot: {1}",
                dir,
                shoot
            );
    }
}