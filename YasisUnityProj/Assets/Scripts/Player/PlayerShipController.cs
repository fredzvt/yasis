﻿using System;
using System.Collections;
using UnityEngine;

public delegate void PlayerShipShootDelegate(PlayerShotController shot);
public delegate void PlayerShipCollideWithAlienShipHandler(GameObject alienShip);

public class PlayerShipController : MonoBehaviour
{
    public event PlayerShipShootDelegate OnShipShoot;
    public event PlayerShipCollideWithAlienShipHandler OnCollideWithAlienShip;

    public ParticleSystem thrustParticles;
    public GameObject ShotPrefab;
    public Transform ShotSpawnPoint;
    public Transform Muzzle;
    public float MuzzleDuration = .1f;
    public bool debug = false;
    public float MovementVelocity = .5f;
    public float MovementDamping = 10f;
    public float RotationRange = 12f;
    public float RotationVelocity = 5f;
    public float RotationDamping = 10f;
    public GameObject explosion;
    public GameObject[] disableOnDestroy;

    private Vector3 initialPosition;
    private float initialRotation;
    private float targetRotation;
    private Vector3 targetPosition;
    private PlayerInput input;
    private bool cannonLocked = false;
    private bool paused = false;
    private bool destroyed = false;
    private Collider[] colliders;
    private Animator explosionAnimator;

    public void OnGUI()
    {
        if (debug)
        {
            GUILayout.TextArea("input: " + input);
            GUILayout.TextArea("targetPosition: " + targetPosition);
            GUILayout.TextArea("transform.position: " + transform.position);
            GUILayout.TextArea("targetRotation: " + targetRotation);
            GUILayout.TextArea("transform.rotation: " + transform.rotation.eulerAngles.y);
        }
    }

    public void Start()
    {
        if (explosion == null) 
            throw new Exception("Explosion is null.");

        if (thrustParticles == null)
            throw new Exception("thrustParticles is null.");

        if (ShotPrefab == null)
            throw new Exception("ShotPrefab is null.");

        if (ShotSpawnPoint == null)
            throw new Exception("ShotSpawnPoint is null.");

        if (Muzzle == null)
            throw new Exception("Muzzle is null.");

        initialPosition = transform.position.Copy();
        targetPosition = transform.position.Copy();
        initialRotation = transform.rotation.y;
        targetRotation = transform.rotation.y;

        explosionAnimator = explosion.GetComponent<Animator>();
        if (explosionAnimator == null)
            throw new Exception("explosionAnimator is null.");

        Muzzle.gameObject.SetActive(false);
        colliders = gameObject.GetComponentsInChildren<Collider>();
    }

    public void Update()
    {
        if (!paused && !destroyed)
        {
            UpdatePlayerInput();
            UpdateCannon();
            UpdateMovement();
            UpdateRotation();
        }
    }

    public void DestroyMe()
    {
        StartCoroutine("ExplodeAndDestroy");
    }

    private IEnumerator ExplodeAndDestroy()
    {
        destroyed = true;

        foreach (var obj in disableOnDestroy)
            obj.SetActive(false);

        foreach (var collider in colliders)
            collider.enabled = false;

        explosion.SetActive(true);
        explosionAnimator.Play("Explosion", -1, 0);

        yield return new WaitForSeconds(.3f);
    }

    public void ResetPaused()
    {
        targetPosition = initialPosition.Copy();
        transform.position = initialPosition.Copy();

        targetRotation = initialRotation;
        transform.rotation =
            Quaternion.Euler(
                transform.rotation.eulerAngles.x,
                initialRotation,
                transform.rotation.eulerAngles.z
            );

        foreach (var obj in disableOnDestroy)
            obj.SetActive(true);

        foreach (var collider in colliders)
            collider.enabled = true;

        explosion.SetActive(false);

        Pause();
        destroyed = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "AlienShip")
        {
            if (OnCollideWithAlienShip != null)
                OnCollideWithAlienShip(other.gameObject);
        }
    }

    public void Pause()
    {
        paused = true;
        thrustParticles.Pause();
    }

    public void Unpause()
    {
        paused = false;
        thrustParticles.Play();
    }

    private void UpdateCannon()
    {
        if (input.shoot && !cannonLocked)
        {
            cannonLocked = true;
            var shot = (GameObject)Instantiate(ShotPrefab, ShotSpawnPoint.position, Quaternion.identity);

            var shotController = shot.GetComponent<PlayerShotController>();
            if (shotController == null)
                throw new Exception("ShotPrefab does not have a PlayerShotController component.");

            shotController.OnDestroyed += ShotDestroyed;

            if (OnShipShoot != null)
                OnShipShoot(shotController);

            StopCoroutine("ShowMuzzle");
            StartCoroutine("ShowMuzzle");
        }
    }

    private IEnumerator ShowMuzzle()
    {
        Muzzle.gameObject.SetActive(true);
        yield return new WaitForSeconds(MuzzleDuration);
        Muzzle.gameObject.SetActive(false);
    }

    private void ShotDestroyed(PlayerShotController shot)
    {
        cannonLocked = false;
    }

    private void UpdateRotation()
    {
        if (input.direction.HasValue &&
            targetPosition.x != GameSettings.PlayerHorizontalRange &&
            targetPosition.x != -GameSettings.PlayerHorizontalRange)
        {
            switch (input.direction.Value)
            {
                case Direction.Left:
                    targetRotation += RotationVelocity;
                    break;

                case Direction.Right:
                    targetRotation -= RotationVelocity;
                    break;

                default:
                    throw new Exception("Unexpected direction.");
            }

            if (targetRotation > RotationRange)
            {
                targetRotation = RotationRange;
            }
            else if (targetRotation < -RotationRange)
            {
                targetRotation = -RotationRange;
            }
        }
        else
        {
            if (targetRotation > 0)
            {
                targetRotation -= RotationVelocity;
            }
            else if (targetRotation < 0)
            {
                targetRotation += RotationVelocity;
            }
        }

        var yAngle =
            Mathf.LerpAngle(
                transform.rotation.eulerAngles.y,
                targetRotation,
                RotationDamping * Time.deltaTime
            );

        transform.rotation =
            Quaternion.Euler(
                transform.rotation.eulerAngles.x,
                yAngle,
                transform.rotation.eulerAngles.z
            );
    }

    private void UpdateMovement()
    {
        targetPosition.y = transform.position.y;

        if (input.direction.HasValue)
        {
            switch (input.direction.Value)
            {
                case Direction.Left:
                    targetPosition.x -= MovementVelocity;
                    break;

                case Direction.Right:
                    targetPosition.x += MovementVelocity;
                    break;

                default:
                    throw new Exception("Unexpected direction.");
            }

            if (targetPosition.x > GameSettings.PlayerHorizontalRange)
            {
                targetPosition.x = GameSettings.PlayerHorizontalRange;
            }
            else if (targetPosition.x < -GameSettings.PlayerHorizontalRange)
            {
                targetPosition.x = -GameSettings.PlayerHorizontalRange;
            }
        }

        if (debug)
        {
            DrawTargetPosition();
        }

        transform.position = 
            Vector3.Lerp(
                transform.position, 
                targetPosition, 
                MovementDamping * Time.deltaTime
            );
    }

    private void DrawTargetPosition()
    {
        var start = targetPosition.Copy();
        start.y -= 1f;

        var end = targetPosition.Copy();
        end.y += 1f;

        Debug.DrawLine(start, end);
    }

    private void UpdatePlayerInput()
    {
        input = new PlayerInput();
        input.shoot = Input.GetKey(GameSettings.PlayerShipShoot);

        var left = Input.GetKey(GameSettings.PlayerShipLeft);
        var right = Input.GetKey(GameSettings.PlayerShipRight);

        if (left && !right)
        {
            input.direction = Direction.Left;
        }
        else if (right && !left)
        {
            input.direction = Direction.Right;
        }
    }
}