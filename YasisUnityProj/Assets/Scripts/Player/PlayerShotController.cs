﻿using System;
using UnityEngine;

public delegate void PlayerShotDestroyedHandler(PlayerShotController shot);
public delegate void PlayerShotHitAlienShipHandler(PlayerShotController shot, GameObject alienShip);

public class PlayerShotController : MonoBehaviour
{
    public float Velocity = 1f;

    public event PlayerShotDestroyedHandler OnDestroyed;
    public event PlayerShotHitAlienShipHandler OnHitAlienShip;

    private bool paused = false;

    public void Update()
    {
        if (!paused)
        {
            var newPos = transform.position.Copy();
            newPos.y += Velocity;
            transform.position = newPos;

            if (transform.position.y > GameSettings.ScreenTop)
            {
                DestroyMe();
            }
        }
    }

    public void Pause()
    {
        paused = true;
    }

    public void Unpause()
    {
        paused = false;
    }

    public void DestroyMe()
    {
        if (OnDestroyed != null) 
            OnDestroyed(this);

        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "AlienShip")
        {
            if (OnHitAlienShip != null)
                OnHitAlienShip(this, other.gameObject);

            DestroyMe();
        }
    }
}