﻿using System;
using UnityEngine;

public static class GameSettings
{
    public static float ScreenLeft = -40f;
    public static float ScreenRight = 40f;
    public static float ScreenTop = 40f;
    public static float ScreenBottom = -8f;

    public static float CameraCraneRotationRange = 6f;

    public static float PlayerHorizontalRange = 22f;

    public static KeyCode PlayerShipLeft = KeyCode.LeftArrow;
    public static KeyCode PlayerShipRight = KeyCode.RightArrow;
    public static KeyCode PlayerShipShoot = KeyCode.LeftControl;
    public static KeyCode PauseRequest = KeyCode.P;

    public static int PointsPorAlienShip = 10;
    public static float RemainingEnergyPerBonus = .01f;
    public static int BonusPerRemainingEnergy = 5;
}
