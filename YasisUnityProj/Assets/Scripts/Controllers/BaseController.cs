﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseController : MonoBehaviour
{
    public Image faderImage;
    public int faderSteps = 10;
    public float faderWaitBetweenSteps = 0;

    private float faderIncrement;
    private float faderCurrValue;
    private Action faderOnCompleteCallback;
    private float faderTargetAlpha;

    public void Awake()
    {
        if (faderImage == null)
            throw new Exception("FaderImage is null.");
    }

    private Color CreateFaderColor(float alpha)
    {
        return new Color(faderImage.color.r, faderImage.color.g, faderImage.color.b, alpha);
    }

    private IEnumerator FadeAlphaToValue()
    {
        faderIncrement = 1f / (float)faderSteps;
        faderCurrValue = 0f;

        for (var i = 0; i < faderSteps; i++)
        {
            faderCurrValue += faderIncrement;
            var newAlpha = Mathf.Lerp(faderImage.color.a, faderTargetAlpha, faderCurrValue);
            var newColor = CreateFaderColor(newAlpha);
            faderImage.color = newColor;
            faderImage.gameObject.SetActive(newAlpha != 0);

            if (faderWaitBetweenSteps > 0)
            {
                yield return new WaitForSeconds(faderWaitBetweenSteps);
            }
            else
            {
                yield return null;
            }
        }

        if (faderOnCompleteCallback != null)
            faderOnCompleteCallback();
    }

    private void FaderToAlpha(float alpha, Action onComplete = null)
    {
        faderOnCompleteCallback = onComplete;
        faderTargetAlpha = alpha;
        StopCoroutine("FadeAlphaToValue");
        StartCoroutine("FadeAlphaToValue");
    }

    protected void FaderToOpaque(Action onComplete = null)
    {
        FaderToAlpha(1f, onComplete);
    }

    protected void FaderToTransparent(Action onComplete = null)
    {
        FaderToAlpha(0, onComplete);
    }
}
