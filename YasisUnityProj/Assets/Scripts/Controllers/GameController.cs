﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using DG.Tweening;
using System.Collections.Generic;

public class GameController : BaseController
{
    public bool debug = false;
    public CameraCraneScript cameraCrane;
    public ParticleSystem starsParticle;
    public GameObject AlienShip1Prefab;
    public PlayerShipController PlayerShip;
    public Image UIEnergyFill;
    public Text UIPoints;
    public Image UILife1;
    public Image UILife2;
    public Image UILife3;
    public Image UILife4;
    public Image UILife5;
    public Text levelIntroLabel;

    private enum States
    {
        FadeInScene,
        FadingInScene,
        IntroducePlayer,
        IntroducingPlayer,
        IntroduceLevel,
        IntroducingLevel,
        GamePlay,
        Paused,
        CountRemainingEnergy,
        CountingRemainingEnergy,
        RefillEnergy,
        RefillingEnergy,
        PlayerDiedPrepareToRespawn,
        PreparingToRespawn,
    }

    private List<AlienShotController> AlienShots = new List<AlienShotController>();
    private List<PlayerShotController> PlayerShots = new List<PlayerShotController>();
    private List<BaseAlienShipController> AlienShips = new List<BaseAlienShipController>();
    private States state;
    private float energy; // Range: 0 to 1
    private int points;
    private int lives;
    private int level;
    private GameObject aliensFolderObject;
    private States stateToGoAfterRefillEnergy = States.IntroduceLevel;
    private Sequence energySequence;
    private Color originalEnergyColor;

    private void ValidateBindings()
    {
        if (cameraCrane == null) throw new Exception("cameraCrane is null.");
        if (starsParticle == null) throw new Exception("starsParticle is null.");
        if (AlienShip1Prefab == null) throw new Exception("AliensShip1Prefab is null.");
        if (PlayerShip == null) throw new Exception("PlayerShip is null.");
        if (UIEnergyFill == null) throw new Exception("UIEnergyFill is null.");
        if (UIPoints == null) throw new Exception("UIPoints is null.");
        if (UILife1 == null) throw new Exception("UILife1 is null.");
        if (UILife2 == null) throw new Exception("UILife2 is null.");
        if (UILife3 == null) throw new Exception("UILife3 is null.");
        if (UILife4 == null) throw new Exception("UILife4 is null.");
        if (UILife5 == null) throw new Exception("UILife5 is null.");
        if (levelIntroLabel == null) throw new Exception("levelIntroLabel is null.");
    }

    public void Start()
    {
        // Validate if all elements were assigned in editor.
        ValidateBindings();

        // Persist original energy bar color.
        originalEnergyColor = UIEnergyFill.color;

        // Hide cursor
        Screen.showCursor = false;

        // Set player event handlers
        PlayerShip.OnShipShoot += OnPlayerShipShoot;
        PlayerShip.OnCollideWithAlienShip += OnCollidePlayerShipWithAlienShip;

        // Initialize DOTween
        DOTween.Init();

        // Initialize game variables
        state = States.FadeInScene;
        energy = 1f;
        points = 0;
        lives = 3;
        level = 1;

        // Update HUD for the first time
        UpdateHUD();

        // Create a "folder" game object to organize alien ships instantiations.
        aliensFolderObject = new GameObject("Alien Ships");
    }

    public void Update()
    {
        // If debug, draw screen bounds on editor.
        if (debug) DebugDrawScreenBounds();

        switch (state)
        {
            case States.FadeInScene:
                PlayerShip.Pause();
                FaderToTransparent(() => state = States.IntroducePlayer);
                state = States.FadingInScene;
                break;

            case States.FadingInScene:
                // Wait for fader callback.
                break;

            case States.IntroducePlayer:
                PlayerShip.transform
                    .DOMoveY(0, 1)
                    .SetEase(Ease.OutExpo)
                    .OnComplete(() => state = States.IntroduceLevel);
                state = States.IntroducingPlayer;
                break;

            case States.IntroducingPlayer:
                // Wait for tweener callback.
                break;

            case States.IntroduceLevel:
                levelIntroLabel.text = "Level " + level;
                levelIntroLabel.gameObject.SetActive(true);
                var seq = DOTween.Sequence();
                seq.Append(levelIntroLabel.DOFade(1, .5f));
                seq.AppendInterval(.5f);
                seq.Append(levelIntroLabel.DOFade(0, .5f));
                seq.OnComplete(() => {
                    PlayerShip.Unpause();
                    InitNewAlienFormation();
                    state = States.GamePlay;
                    levelIntroLabel.gameObject.SetActive(false);
                });
                seq.Play();
                state = States.IntroducingLevel;
                break;

            case States.IntroducingLevel:
                // Wait for tweener callback.
                break;

            case States.GamePlay:
                CheckForPauseRequests();
                ConsumeEnergy();
                CheckLevelClearing();
                UpdateHUD();
                break;

            case States.Paused:
                CheckForUnpauseRequests();
                break;

            case States.PlayerDiedPrepareToRespawn:
                PrepareToRespawn();
                state = States.PreparingToRespawn;
                break;

            case States.PreparingToRespawn:
                // Waiting coroutine.
                break;

            case States.CountRemainingEnergy:
                StartCoroutine("CountRemainingEnergy");
                state = States.CountingRemainingEnergy;
                break;

            case States.CountingRemainingEnergy:
                UpdateHUD();
                // Waiting coroutine.
                break;

            case States.RefillEnergy:
                StartCoroutine("RefillEnergy");
                state = States.RefillingEnergy;
                break;

            case States.RefillingEnergy:
                UpdateHUD();
                // Waiting coroutine.
                break;

            default:
                throw new Exception("Unexpected game state!");
        }
    }

    private void CheckLevelClearing()
    {
        if (AlienShips.Count == 0)
        {
            level++;
            state = States.CountRemainingEnergy;
        }
    }

    private void PrepareToRespawn()
    {
        foreach (var ship in AlienShips)
            ship.Stop();

        for (var i = PlayerShots.Count - 1; i >= 0; i--)
            PlayerShots[i].DestroyMe();

        for (var i = AlienShots.Count - 1; i >= 0; i--)
            AlienShots[i].DestroyMe();

        var height = GameSettings.ScreenTop - GameSettings.ScreenBottom;
        var seq = DOTween.Sequence();
        seq.AppendInterval(1);

        foreach (var ship in AlienShips)
        {
            var tween = ship.transform.DOMoveY(ship.transform.position.y + height, 2);
            tween.SetEase(Ease.InCubic);
            seq.Insert(1, tween);
        }

        seq.OnComplete(() =>
        {
            for (var i = AlienShips.Count - 1; i >= 0; i--)
                RemoveAndDestroyAlienShip(AlienShips[i]);

            PlayerShip.ResetPaused();
            stateToGoAfterRefillEnergy = States.IntroducePlayer;
            state = States.RefillEnergy;
        });

        seq.Play();
    }

    private IEnumerator CountRemainingEnergy()
    {
        while (energy > 0)
        {
            energy -= GameSettings.RemainingEnergyPerBonus;
            points += GameSettings.BonusPerRemainingEnergy;
            yield return new WaitForSeconds(.01f);
        }

        energy = 0;
        state = States.RefillEnergy;
    }

    private IEnumerator RefillEnergy()
    {
        if (energySequence != null)
            energySequence.Kill();

        UIEnergyFill.color = originalEnergyColor;

        while (energy < 1)
        {
            energy += GameSettings.RemainingEnergyPerBonus;
            yield return new WaitForSeconds(.01f);
        }

        energy = 1f;
        state = stateToGoAfterRefillEnergy;
    }

    private void ConsumeEnergy()
    {
        var consume = Time.deltaTime / 60;
        energy -= consume;

        if (energy < .2f && energySequence == null)
        {
            UIEnergyFill.color = Color.red;
            energySequence = DOTween.Sequence();
            energySequence.Append(UIEnergyFill.DOFade(.5f, .3f));
            energySequence.Append(UIEnergyFill.DOFade(1, .3f));
            energySequence.SetLoops(-1);
            energySequence.Play();
        }

        if (energy < 0)
        {
            energy = 0;
            DestroyPlayerShip();
        }
    }

    private void CheckForPauseRequests()
    {
        if (Input.GetKeyDown(GameSettings.PauseRequest))
        {
            Pause();
            state = States.Paused;
        }
    }

    private void CheckForUnpauseRequests()
    {
        if (Input.GetKeyDown(GameSettings.PauseRequest))
        {
            Unpause();
            state = States.GamePlay;
        }
    }

    private void Pause()
    {
        levelIntroLabel.text = "PAUSED";
        levelIntroLabel.gameObject.SetActive(true);
        levelIntroLabel.color =
            new Color(
                levelIntroLabel.color.r,
                levelIntroLabel.color.g,
                levelIntroLabel.color.b,
                1f
            );

        cameraCrane.Pause();
        starsParticle.Pause();
        PlayerShip.Pause();

        foreach (var shot in PlayerShots)
            shot.Pause();

        foreach (var shot in AlienShots)
            shot.Pause();

        foreach (var alienShip in AlienShips)
            alienShip.Pause();
    }

    private void Unpause()
    {
        levelIntroLabel.gameObject.SetActive(false);

        cameraCrane.Unpause();
        starsParticle.Play();
        PlayerShip.Unpause();

        foreach (var shot in PlayerShots)
            shot.Unpause();

        foreach (var shot in AlienShots)
            shot.Unpause();

        foreach (var alienShip in AlienShips)
            alienShip.Unpause();
    }

    private void DebugDrawScreenBounds()
    {
        var topLeftVec = new Vector3(GameSettings.ScreenLeft, GameSettings.ScreenTop);
        var topRightVec = new Vector3(GameSettings.ScreenRight, GameSettings.ScreenTop);
        var bottomLeftVec = new Vector3(GameSettings.ScreenLeft, GameSettings.ScreenBottom);
        var bottomRightVec = new Vector3(GameSettings.ScreenRight, GameSettings.ScreenBottom);

        Debug.DrawLine(topLeftVec, topRightVec, Color.red);
        Debug.DrawLine(topLeftVec, bottomLeftVec, Color.red);
        Debug.DrawLine(bottomRightVec, topRightVec, Color.red);
        Debug.DrawLine(bottomRightVec, bottomLeftVec, Color.red);
    }

    private void UpdateHUD()
    {
        var scale = new Vector3(energy, 1f, 1f);
        UIEnergyFill.rectTransform.localScale = scale;

        UIPoints.text = points.ToString();

        UILife1.gameObject.SetActive(lives >= 1);
        UILife2.gameObject.SetActive(lives >= 2);
        UILife3.gameObject.SetActive(lives >= 3);
        UILife4.gameObject.SetActive(lives >= 4);
        UILife5.gameObject.SetActive(lives >= 5);
    }

    private void InitNewAlienFormation()
    {
        var horSpaceBetweenShips = 10f;
        var halfHorSpaceBetweenShips = horSpaceBetweenShips / 2;
        var vertSpaceBetweenLines = 4f;
        var paddingTop = 15;
        var paddingRight = 5;

        for (var row = 0; row < 3; row++)
        {
            var even = row % 2 == 0;
            for (var col = 0; col < 8; col++)
            {
                var x = GameSettings.ScreenLeft - (horSpaceBetweenShips * col) - paddingRight;
                if (even) x -= halfHorSpaceBetweenShips;

                var y = GameSettings.ScreenTop - (vertSpaceBetweenLines * row) - paddingTop;

                var alienShipObj = (GameObject)Instantiate(AlienShip1Prefab, new Vector3(x, y), Quaternion.identity);
                alienShipObj.transform.parent = aliensFolderObject.transform;
                var alienShip = alienShipObj.GetComponent<BaseAlienShipController>();

                if (alienShip == null)
                    throw new Exception("AlienShip1Prefab does not contain a BaseAlienShipController component.");

                alienShip.level = level;
                alienShip.OnShipShoot += OnAlienShipShoot;
                AlienShips.Add(alienShip);
            }
        }
    }

    private void OnPlayerShotDestroyed(PlayerShotController shot)
    {
        PlayerShots.Remove(shot);
    }

    private void RemoveAndDestroyAlienShip(GameObject alienShipGameObject)
    {
        var alienShip = alienShipGameObject.GetComponent<BaseAlienShipController>();

        if (alienShip == null)
            throw new Exception("alienShip does not have a BaseAlienShipController component attached.");

        RemoveAndDestroyAlienShip(alienShip);
    }

    private void RemoveAndDestroyAlienShip(BaseAlienShipController alienShip)
    {
        AlienShips.Remove(alienShip);
        alienShip.DestroyMe();
    }

    private void DestroyPlayerShip()
    {
        PlayerShip.DestroyMe();
        state = States.PlayerDiedPrepareToRespawn;
    }

    private void OnPlayerShotHitAlienShip(PlayerShotController shot, GameObject alienShipGameObject)
    {
        points += GameSettings.PointsPorAlienShip;
        RemoveAndDestroyAlienShip(alienShipGameObject);
    }

    private void OnAlienShipShoot(AlienShotController shot)
    {
        AlienShots.Add(shot);
        shot.OnDestroyed += OnAlienShotDestroyed;
        shot.OnHitPlayerShip += OnAlienShotHitPlayerShip;
    }

    private void OnAlienShotDestroyed(AlienShotController shot)
    {
        AlienShots.Remove(shot);
    }

    private void OnPlayerShipShoot(PlayerShotController shot)
    {
        PlayerShots.Add(shot);
        shot.OnDestroyed += OnPlayerShotDestroyed;
        shot.OnHitAlienShip += OnPlayerShotHitAlienShip;
    }

    private void OnAlienShotHitPlayerShip(AlienShotController shot)
    {
        DestroyPlayerShip();
    }

    private void OnCollidePlayerShipWithAlienShip(GameObject alienShipGameObject)
    {
        RemoveAndDestroyAlienShip(alienShipGameObject);
        DestroyPlayerShip();
    }
}