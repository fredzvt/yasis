﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TitleController : BaseController
{
    public void Start()
    {
        FaderToTransparent();

        // Show cursor
        Screen.showCursor = true;
    }

    public void HandleStartButtonPress()
    {
        FaderToOpaque(() => Application.LoadLevel("Main Scene"));
    }
}
