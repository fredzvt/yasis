﻿using System;
using UnityEngine;

public static class Utils
{
    public static Vector3 Copy(this Vector3 v)
    {
        var newVec = new Vector3(v.x, v.y, v.z);
        return newVec;
    }

    public static Quaternion Copy(this Quaternion q)
    {
        var newQuaternion = new Quaternion(q.x, q.y, q.z, q.w);
        return newQuaternion;
    }

    public static void DebugDrawCrossAtPosition(Vector3 pos, Color color, float size = .5f)
    {
        var topLeft = new Vector3(pos.x - size, pos.y + size);
        var topRight = new Vector3(pos.x + size, pos.y + size);
        var bottomLeft = new Vector3(pos.x - size, pos.y - size);
        var bottomRight = new Vector3(pos.x + size, pos.y - size);

        Debug.DrawLine(topLeft, bottomRight, color);
        Debug.DrawLine(bottomLeft, topRight, color);
    }
}